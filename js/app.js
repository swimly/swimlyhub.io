/**
 * Created by 97974 on 2016/5/26.
 */
var app=angular.module("App",
		['ngAnimate',
		 'ui.router',
		 'app.controllers',
		 'app.services',
		 'app.directive',
		 'app.config',
		 'ngResource',
		 'naif.base64'
		 ])
.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('home', {
            url: "/home",
            views:{
                "main":{
                    templateUrl: "templates/home/home.html",
                    controller:"homeController"
                }
            }
        })
        .state('home-banner',{
            url:"/home/banner",
            views:{
                "main":{
                    templateUrl:"templates/pages/home-banner.html",
                    controller:"homeBannerController"
                }
            }
        })
        .state('home-article',{
            url:"/home/article",
            views:{
                "main":{
                    templateUrl:"templates/pages/home-article.html",
                    controller:"homeArticleController"
                }
            }
        })
        .state('home-project',{
            url:"/home/project",
            views:{
                "main":{
                    templateUrl:"templates/pages/home-project.html",
                    controller:"homeProjectController"
                }
            }
        })
        .state('home-about',{
            url:"/home/about",
            views:{
                "main":{
                    templateUrl:"templates/pages/home-about.html",
                    controller:"homeAboutController"
                }
            }
        })
        .state('project', {
            url: "/project",
            views:{
                "main":{
                    controller:"projectController",
                    templateUrl: "templates/project/project.html"
                }
            }
        })
        .state('projectInfo', {
            url: "/project/:id",
            views:{
                "main":{
                    templateUrl: "templates/project/project-info.html",
                    controller:"projectInfoController"
                }
            }
        })
        .state('article', {
            url: "/article",
            views:{
                "main":{
                    templateUrl: "templates/article/article.html",
                    controller:"articleController"
                }
            }
        })
        .state("articleInfo",{
            url:"/article/:id",
            views:{
                "main":{
                    templateUrl: "templates/article/article-info.html",
                    controller:"articleInfoController"
                }
            }
        })
        .state('about', {
            url: "/about",
            views:{
                "main":{
                    templateUrl: "templates/about/about.html"
                }
            }
        })
        .state('admin', {
            url: "/admin",
            views:{
                "main":{
                    templateUrl: "templates/admin/index.html"
                }
            }
        })
        .state('admin.addProject', {
            url: "/admin/addProject",
            views:{
                "content":{
                    templateUrl: "templates/admin/addProject.html",
                    controller:"addProjectController"
                }
            }
        })
        $urlRouterProvider.otherwise("/home");
})
