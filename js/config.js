/**
 * Created by 97974 on 2016/5/29.
 */
var config=angular.module("app.config",[])
    .constant("config",{
        site:"http://127.0.0.1/api/",/*http://www.liuwbox.com:8081/app/*/
        resource:"https://github.com/swimly/",
        projects:"api.php",
        projectInfo:"projectsInfo.php",
        upload:"uploads.php"
    })
