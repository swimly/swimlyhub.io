/**
 * Created by 97974 on 2016/5/26.
 */
var services=angular.module('app.controllers', [])
.controller("main",["$scope","$rootScope",function($scope,$rootScope){
    $rootScope.isProject=false;
}])
.controller("homeController",["$scope","$rootScope",function($scope,$rootScope){
    $rootScope.isProject=false;
    $.getScript('js/FSS.js',function(){
    	FSS("container", "output")
    });
}])
.controller("homeBannerController",["$scope",function($scope){
    $scope.pageClass = 'home-banner';
    $scope.sportImages=["img/banner1.webp",'img/banner2.webp','img/banner3.webp','img/banner4.webp','img/banner5.webp','img/banner6.webp']
}])
.controller("homeArticleController",["$scope",function($scope){
    $scope.pageClass = 'home-article';
}])
.controller("homeProjectController",["$scope",function($scope){
    $scope.pageClass = 'home-project';
}])
.controller("homeAboutController",["$scope",function($scope){
    $scope.pageClass = 'home-about';
}])
.controller("addProjectController",["$scope","$http","$window","$rootScope","config","addProjectFac","$filter",function($scope, $http, $window, $rootScope,config,addProjectFac,$filter){
	var api=config.site+config.upload;
	var uploadedCount = 0;
	$scope.isUploaded=false;
    $scope.files = [];
    $scope.time=new Date();
    $scope.time=$filter("date")($scope.time, "yyyy-MM-dd HH:mm:ss");
    $scope.data={
    	title:"",
    	type:"",
    	thumb:$scope.files[0],
    	url:"",
    	resource:"",
    	introduce:"",
    	time:$scope.time
    }
    $scope.file = {};
    /*头像上传*/
    $scope.uploadFiles = function() {
      var files = angular.copy($scope.files);
      if ($scope.file) {
        files.push($scope.file);
      }
      if (files.length === 0) {
        $window.alert('Please select files!');
        return false;
      }
      console.log(files)
      for (var i = files.length - 1; i >= 0; i--) {
        var file = files[i];
        $http.post(api, file)
        .success(function(res){
          uploadedCount ++;
          if (uploadedCount == files.length) {
          }
          $scope.isUploaded=true;
          $scope.data.thumb=res;
        })
        .error(function(err){
        });
      }
    };
    /*获取表单数据*/
    $scope.send=function(){
    	addProjectFac.send($scope.data);
    }
}])
/*项目列表*/
.controller("projectController",["$scope","config","projectFac","$rootScope",function($scope,config,projectFac,$rootScope){
    $scope.loading=true;
    $scope.loadText="加载更多"
    $rootScope.projectMain=true;
    $scope.loadTextShow=false;
    $scope.loadmore=false;
    $scope.hasdata=false;
    projectFac.getProjects();
    $scope.$on("projects.get.success",function(event,data){
        $scope.data=projectFac.getBackData();
        console.log()
        if(typeof $scope.data=='undefined'){
            $scope.loadTextShow=true;
            $scope.loading=false;
            $scope.loadText="没有数据！"
            $scope.loadMore=null;
        }else{
            $scope.loading=false;
            $scope.loadTextShow=true;
            $scope.hasdata=true;
            console.log($scope.data);
        }
    })
    $scope.$on("projects.getMore.success",function(event,data){
        $scope.data=projectFac.getMoreBack();
        $scope.loadText="加载更多"
        $scope.loadmore=false;
        /*console.log($scope.data)*/
    })
    $scope.$on("projects.getMore.failded",function(event,data){
        $scope.loadMore=function(){
            $scope.loadText="没有更多了"
        }
    })
    $scope.loadMore=function(){
        projectFac.getMoreProjects();
        $scope.$broadcast("get.more.complete");
        $scope.loadText="加载中……";
        $scope.loadmore=true;
    }
}])
/*项目详情*/
.controller("projectInfoController",["$scope","$stateParams","projectInfoFac","config","$rootScope",function($scope,$stateParams,projectInfoFac,config,$rootScope){
    $scope.loading=true;
    $rootScope.isProject=true;
    var id=$stateParams.id;
    projectInfoFac.get(id);
    $scope.$on("projectInfo.get.sucess",function(){
        $scope.data=projectInfoFac.getInfo();
        $scope.loading=false;
        console.log($scope.data)
    })
}])
/*文章*/
.controller("articleController",["$scope","config","articleFac","$rootScope",function($scope,config,articleFac,$rootScope){
    $scope.loading=true;
    $scope.loadText="加载更多"
    $rootScope.projectMain=true;
    $scope.loadTextShow=false;
    $scope.loadmore=false;
    $scope.hasdata=false;
    articleFac.getProjects();
    $scope.$on("articles.get.success",function(event,data){
        $scope.data=articleFac.getBackData();
        console.log()
        if(typeof $scope.data=='undefined'){
            $scope.loadTextShow=true;
            $scope.loading=false;
            $scope.loadText="没有数据！"
            $scope.loadMore=null;
        }else{
            $scope.loading=false;
            $scope.loadTextShow=true;
            $scope.hasdata=true;
            console.log($scope.data);
        }
    })
    $scope.$on("articles.getMore.success",function(event,data){
        $scope.data=articleFac.getMoreBack();
        $scope.loadText="加载更多"
        $scope.loadmore=false;
        /*console.log($scope.data)*/
    })
    $scope.$on("articles.getMore.failded",function(event,data){
        $scope.loadMore=function(){
            $scope.loadText="没有更多了"
        }
    })
    $scope.loadMore=function(){
        articleFac.getMoreProjects();
        $scope.$broadcast("get.more.complete");
        $scope.loadText="加载中……";
        $scope.loadmore=true;
    }
}])
/*文章详情*/
.controller("articleInfoController",["$scope","$stateParams","articleInfoFac","config","$rootScope",function($scope,$stateParams,articleInfoFac,config,$rootScope){
    $scope.loading=true;
    /*$rootScope.isProject=true;*/
    var id=$stateParams.id;
    articleInfoFac.get(id);
    $scope.$on("articleInfo.get.sucess",function(){
        $scope.data=articleInfoFac.getInfo();
        $scope.loading=false;
        console.log($scope.data)
    })
}])