/**
 * Created by 97974 on 2016/5/26.
 */
angular.module('app.services', [])
.factory("projectFac",function($resource,$rootScope,config){
    var getUrl=config.site+config.projects;
    var types="getProjects";
    var projectsList={};
    var resource = $resource(getUrl, {}, {
        query: {
            method: 'get',
            params: {
                page: '@page',
                type:"@type"
            },
            timeout: 20000
        }
    });
    return {
        getProjects:function(){
            var hasNextPage=true;
            resource.query({
                page:1,
                type:types
            }, function (r) {
                projectsList={
                    "nextPage":2,
                    "hasNextPage":hasNextPage,
                    "pageCount": r.pageCount,
                    "data":r.list
                }
                //数据请求完成通知controller
                $rootScope.$broadcast("projects.get.success");
            })
        },
        getBackData:function(){
            if(projectsList=="undefined"){
                return false;
            }else{
                if(projectsList.nextPage){
                    return projectsList.data;
                }
            }
        },
        getMoreProjects:function(){
            //获取之前的数据
            if(projectsList==undefined){
                return false;
            }
            var hasNextPage=projectsList.hasNextPage;
            var nextPage=projectsList.nextPage;
            var moreData=projectsList.data;
            var pageCount=projectsList.pageCount;
            resource.query({
                page:nextPage,
                type:types
            }, function (r) {
                nextPage++;
                console.log(nextPage,pageCount)
                if(nextPage>pageCount){
                    hasNextPage=false;
                    /*console.log(nextPage==pageCount,nextPage,pageCount);*/
                    $rootScope.$broadcast("projects.getMore.failded");
                }
                moreData=moreData.concat(r.list);
                projectsList={
                    "hasNextPage":hasNextPage,
                    "nextPage":nextPage,
                    "pageCount": r.pageCount,
                    "data":moreData
                }
                //数据请求完成通知controller
                /*console.log(projectsList)*/
                $rootScope.$broadcast("projects.getMore.success");
            })
        },
        getMoreBack:function(){
            if(projectsList=="undefined"){
                return false;
            }else{
                if(projectsList.nextPage){
                    return projectsList.data;
                }
            }
        }
    }
})
.factory("projectInfoFac",function($resource,$rootScope,config){
    var apiUrl=config.site+config.projects,
        info,
        type="getProjectInfo",
        resource=$resource(apiUrl,{},{
        query:{
            method:"get",
            params:{
                id:"@id",
                type:"@type"
            },
            timeout:20000
        }
    });
    return{
        get:function(id){
            return resource.query({
                id:id,
                type:type
            },function(response){
                info=response;
                $rootScope.$broadcast("projectInfo.get.sucess");
            })
        },
        getInfo:function(){
            return info;
        }
    }
})
.factory("articleFac",function($resource,$rootScope,config){
    var getUrl=config.site+config.projects;
    var types="getArticles";
    var projectsList={};
    var resource = $resource(getUrl, {}, {
        query: {
            method: 'get',
            params: {
                page: '@page',
                type:"@type"
            },
            timeout: 20000
        }
    });
    return {
        getProjects:function(){
            var hasNextPage=true;
            resource.query({
                page:1,
                type:types
            }, function (r) {
                projectsList={
                    "nextPage":2,
                    "hasNextPage":hasNextPage,
                    "pageCount": r.pageCount,
                    "data":r.list
                }
                //数据请求完成通知controller
                $rootScope.$broadcast("articles.get.success");
            })
        },
        getBackData:function(){
            if(projectsList=="undefined"){
                return false;
            }else{
                if(projectsList.nextPage){
                    return projectsList.data;
                }
            }
        },
        getMoreProjects:function(){
            //获取之前的数据
            if(projectsList==undefined){
                return false;
            }
            var hasNextPage=projectsList.hasNextPage;
            var nextPage=projectsList.nextPage;
            var moreData=projectsList.data;
            var pageCount=projectsList.pageCount;
            resource.query({
                page:nextPage,
                type:types
            }, function (r) {
                nextPage++;
                console.log(nextPage,pageCount)
                if(nextPage>pageCount){
                    hasNextPage=false;
                    /*console.log(nextPage==pageCount,nextPage,pageCount);*/
                    $rootScope.$broadcast("articles.getMore.failded");
                }
                moreData=moreData.concat(r.list);
                projectsList={
                    "hasNextPage":hasNextPage,
                    "nextPage":nextPage,
                    "pageCount": r.pageCount,
                    "data":moreData
                }
                //数据请求完成通知controller
                /*console.log(projectsList)*/
                $rootScope.$broadcast("articles.getMore.success");
            })
        },
        getMoreBack:function(){
            if(projectsList=="undefined"){
                return false;
            }else{
                if(projectsList.nextPage){
                    return projectsList.data;
                }
            }
        }
    }
})
.factory("articleInfoFac",function($resource,$rootScope,config){
    var apiUrl=config.site+config.projects,
        info,
        type="getarticleInfo",
        resource=$resource(apiUrl,{},{
            query:{
                method:"get",
                params:{
                    id:"@id",
                    type:"@type"
                },
                timeout:20000
            }
        });
    return{
        get:function(id){
            return resource.query({
                id:id,
                type:type
            },function(response){
                info=response;
                $rootScope.$broadcast("articleInfo.get.sucess");
            })
        },
        getInfo:function(){
            return info;
        }
    }
})
.factory("addProjectFac",function($resource,$rootScope,config){
	var apiUrl=config.site+config.projects,
		type="insertProjects";
	resource=$resource(apiUrl,{},{
        query:{
            method:"get",
            params:{
            	data:"@data",
                type:"@type"
            },
            timeout:20000
        }
    });
	return {
		send:function(data){
			return resource.query({
				data:data,
                type:type
            },function(response){
            	console.log(response)
            })
		}
	}
})